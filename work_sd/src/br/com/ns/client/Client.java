/*
 * Trabalho de Sistemas distribuídos.
 * Por Bruno R. holanda, Campo Grande - MS, Brasil.
 */
package br.com.ns.client;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
/**
 * Classe que é responsável por enviar mensagens para o grupo multicast.
 * @author Bruno R. Holanda
 */
public class Client {
    /**
     * Armazena o socket de conexão com o servidor de nomes.
     */
    private Socket socket;
    /**
     * Armazena o buffer de saída para o servidor de nomes.
     */
    private PrintStream saida;
    /**
     * Armazena o buffer de entrada para o servidor de nomes.
     */
    private Scanner entrada;
    /**
     * Armazena o endereço do servidor de nomes.
     */
    private final String hostName;
    /**
     * Armazena a porta de acesso para o processo servidor do servidor de nomes.
     */
    private final int port;
    /**
     * Armazena o socket de conexão com o coordenador da rede multicast.
     */
    private Socket coordenador;
    /**
     * Thread de leitura para tratar as respostas do servidor de nomes.
     */
    private Thread reader;
    /**
     * Crie um instância de Client informando o endereço e a porta de conexão com o servidor de nomes.
     * @param hostName endereço do servidor de nomes.
     * @param port  porta de acesso ao processo do servidor de nomes.
     */
    public Client( String hostName, int port ) {
        this.hostName = hostName;
        this.port = port;
        this.coordenador = null;
    }
    /**
     * Envia uma mensagem para o servidor de nomes.
     * @param message mensagem no formato: função[:param[1]:param[i+1]:param[n]].
     */
    public void send( String message ) {
        this.saida.println( message );
    }
    /**
     * Envia uma mensagem para o grupo multicast.
     * Caso o servidor de nomes ainda não tenha sincronizado o coordenador a função força o reenvio da mensagem por recursão.
     * @param message mensagem no formato: função:ID_NOGrupo:mensagem.
     */
    public void sendCoordenador( String message ) {
        try {
            PrintStream ps = new PrintStream(
                    this.coordenador.getOutputStream()
            );
            ps.println( message );
        } catch (IOException ex) {
            System.out.println("Erro no canal TCP: " + ex);
            System.out.println("Requisitando endereço do coordenador");
            this.coordenador = null;
            this.getCoordenador();
            System.out.println("Aguardando Servidor de Nomes!");
            
            int i = 3;
            while (( i > 0 ) && ( this.coordenador == null )) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex1) {
                    System.out.println("NoGrupo: Interrupção Inesperada !\n" + ex1);
                }
                i--;
            }
            if ( i > 0) {
                this.sendCoordenador(message);
            }
        }
    }
    /**
     * Envia uma solicitação para sincronizar o corrdenador do multicast ao servidor de nomes.
     */
    public void getCoordenador() {
        this.send( "getCoordenador" );
    }
    /**
     * Inicia a conexão com o grupo multicast.
     * Se conecta com o servidor de nomes e incia o thread que trata as respostas do mesmo;
     * Busca pelo coordenador caso o servidor de nomes demore muito tempo para responder
     * gera um erro de timeout.
     */
    public void conect() {
        try {
            System.out.println("Iniciando conexão com o Servidor de Nomes !");
            this.socket = new Socket( this.hostName, this.port );
            this.saida = new PrintStream(this.socket.getOutputStream());
            this.entrada = new Scanner(this.socket.getInputStream());
            this.reader = new Thread(new Reader(this));
            
            this.reader.start();
            this.getCoordenador();
            
            System.out.println("Aguardando Servidor de Nomes!");
            
            int i = 5;
            while (( i > 0 ) && ( this.coordenador == null )) {
                Thread.sleep(100);
                i--;
            }
            if (i == 0) {
                System.out.println("Timeout exception: O servidor de nomes demorou muito para responder !");
                System.exit(0);
            }else {
                System.out.println("Conectado !");
            }
        } catch ( IOException ex ) {
            System.out.println("Erro no canal TCP");
            System.exit(0);
        } catch (InterruptedException ex) {
            System.out.println("Interrupção Inesperada !\n" + ex);
        }
    }
    /**
     * Classe que trata as respostas do servidor de nomes.
     */
    protected class Reader implements Runnable {
        /**
         * armazena a instância atual do client.
         */
        private final Client client;
        /**
         * armazena a thread que trata as respostas do coordenador.
         */
        private Thread coordenadorReader;
        
        /**
         * Inicia o objeto Reader com uma instância válida do objeto cliente.
         * @param client objeto de comunicação com o grupo multcast.
         */
        public Reader( Client client ) {
            this.client = client;
            this.coordenadorReader = null;
        }
        /**
         * Método Thread.
         * caso setCoordenador: seta na instância cliente o 
         *          objeto socket do coordenador com o ip e porta 
         *          do coordenador do grupo multcast. Caso exista um instância
         *          o algoritmo finaliza a mesma usando o método join().
         * caso 200: sucesso na requisição.
         * caso 500: Erro interno da aplicação.
         * caso 404: comando não encontrado.
         */
        @Override
        public void run() {
            while ( this.client.entrada.hasNext() ) {
                
                try {
                    String response = this.client.entrada.nextLine();
                    
                    String[] arg = response.split(";");
                    
                    if ( arg[0] != null ) switch ( arg[0] ) {
                        case "setCoordenador":
                            
                            this.client.coordenador = new Socket(arg[2], 
                                    Integer.valueOf(arg[1])
                            );
                            System.out.println();
                            System.out.println("coord porta:" + arg[1]);
                            System.out.println("coord IP: " + arg[2]);
                            System.out.print("Mensagem: ");
                            if( this.coordenadorReader != null){
                                this.coordenadorReader.join();
                            }
                            this.coordenadorReader = new Thread(
                                    new CoordenadorReader(
                                            this.client.coordenador)
                            );
                            this.coordenadorReader.start();
                            break;
                        case "200" :
                            System.out.println( "Servidor de Nomes: SUCESSO !" );
                            break;
                        case "500" :
                            System.out.println( "Servidor de Nomes: ERRO INTERNO !" );
                            break;
                        case "404" :
                            System.out.println( "Servidor de Nomes: FUNÇÃO DESCONHECIDA" );
                            break;
                    }
                } catch (IOException ex) {
                    System.out.println( "Cliente: Erro no canal TCP" );
                }catch (InterruptedException ex) {
                    System.out.println( "Cliente: Interrupção inesperada" );
                }
            }
        }
    } 
    /**
     * Classe que trata as resposta do coordenador
     */
    protected class CoordenadorReader implements Runnable {
        
        private final Socket coordenador;

        public CoordenadorReader( Socket coordenador ) {
            this.coordenador = coordenador;
        }    
        /**
         * imprime na tela do cliente as resposta do coordenador.
         */
        @Override
        public void run() {
            try {
                Scanner s = new Scanner(this.coordenador.getInputStream());
                
                while (s.hasNext() ) {
                    System.out.println(s.nextLine());
                }
            } catch (IOException ex) {
                System.out.println("Cliente: Erro no canal TCP: " + ex);
            }
        }
    }
    /**
     * Método principal.
     * @param args 
     */
    public static void main(String[] args) {
        Client c = new Client("localhost", 8090);
        c.conect();
        Scanner s = new Scanner(System.in);
        while (s.hasNext()) {
            String[] str = s.nextLine().split(":");
            c.sendCoordenador("mensagem;" + str[0] + ";" + str[1]);
            System.out.print("Mensagem: ");
        }
    }
}
