/*
 * Trabalho de Sistemas distribuídos.
 * Por Bruno R. holanda, Campo Grande - MS, Brasil.
 */
package br.com.ns.server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/**
 * Classe que é responsável por tratar o cliente e o coordenador do grupo multicast.
 * @author Bruno R. Holanda
 */
public class ServerNS {
    private int porta;
    private List<PrintStream> clientes;
    private int coordenadorPorta;
    private String coordenadorIP;
    
    public ServerNS (int porta) {
        this.porta = porta;
        this.coordenadorPorta = 0;
        this.coordenadorIP = null;
        this.clientes = new ArrayList<>();
    }
    /**
     * Envia uma mensagem via broadcast para todos os clientes com conexão válidas.
     * @param msg mensagem do formato: comando[:para[1]:param[i+1]:param[n]]
     */
    public void sendBroadcast(String msg) {
        for (PrintStream ps : clientes) {
            if (!ps.checkError()) {
                ps.println(msg);
            }
        }
    }
    /**
     * Inicia o servidor e fica aceitando clientes
     * @throws IOException 
     */
    public void start () throws IOException {
        ServerSocket servidor = new ServerSocket(this.porta);
        System.out.println("porta " + this.porta + " Aberta !");
        
        while(true){
            Socket cliente = servidor.accept();
            System.out.println("Nova conexão com cliente " + 
                    cliente.getInetAddress().getHostAddress()
            );
            DealClient deal = new DealClient(cliente, this);
            Thread t = new Thread(deal);
            t.start();
            this.clientes.add(new PrintStream(cliente.getOutputStream()));
        }
    }
    /**
     * Classe que trada as requisições dos clientes
     */
    public class DealClient implements Runnable {
        
        private Socket cliente;
        private ServerNS servidor;
        
        public DealClient( Socket cliente, ServerNS servidor ) {
            this.cliente = cliente;
            this.servidor = servidor;
        }
        /**
         * Trata os processos que desejam saber o endereço do coordenador do grupo
         * ou o processo do coordenador que deseja atualizar o endereço do coordenador
         * @Receive: function;param(1);param(2);param(n+1);
         * @retorna: 200 - Sucesso, 404 - não encontrado, 500 - Erro interno, coordInetAddress;
         */
        @Override
        public void run ( ){
            try {
                Scanner s = new Scanner( this.cliente.getInputStream () );
                
                PrintStream saida = new PrintStream(
                        this.cliente.getOutputStream()
                );
                
                while( s.hasNextLine () ) {
                    
                    String receive = s.nextLine();
                    
                    String[] arg = receive.split(";");
                    
                    if (null != arg[0]) switch (arg[0]) {
                        
                        case "getCoordenador" :
                            if ( this.servidor.coordenadorIP != null ) {
                                saida.println("setCoordenador;" 
                                        + this.servidor.coordenadorPorta + ";"
                                        + this.servidor.coordenadorIP + ";"
                                );
                            } else {
                                saida.println("500");
                            }
                            break;
                        case "setCoordenador" :
                            this.servidor.coordenadorPorta = Integer.valueOf(arg[1]);
                            this.servidor.coordenadorIP = arg[2];
                            System.out.println("porta:" + arg[1]);
                            System.out.println("IP: " + arg[2]);
                            saida.println("200");
                            this.servidor.sendBroadcast("setCoordenador;" 
                                        + this.servidor.coordenadorPorta + ";"
                                        + this.servidor.coordenadorIP + ";"
                            );
                            break;
                        default:
                            saida.println("404");
                    }
                }

            }   catch (IOException ex) {
                System.out.println("Erro");
            }
        }
        
    }
    public static void main(String[] args) throws IOException {
        ServerNS servidor = new ServerNS(8090);
        servidor.start();
    }
}
