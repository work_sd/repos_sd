/*
 * Trabalho de Sistemas distribuídos.
 * Por Bruno R. holanda, Campo Grande - MS, Brasil.
 */
package br.com.multicast.group;

import java.io.IOException;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Classe que é responsável por tratar o cliente e o coordenador do grupo multicast.
 * @author Bruno R. Holanda
 */
public class NoGrupo {
    
    private int id;
    private int IdCoordenador;
    private final int porta;
    private final MulticastSocket grupo;
    private InetAddress enderecoGrupo;
    private Thread reader;
    private Thread coordenador;
    private Thread verify;
    private boolean election = false;
    private boolean won = false;
    private boolean iCoordedador = false;
    private Calendar timeCoordenador;
    
    /**
     * Informe o id do nó e a porta.
     * @param id Identificador do processo no grupo multicast.
     * @param porta porta de acesso ao grupo multicast.
     * @throws IOException no caso de haver alguma irregularidade.
     */
    public NoGrupo( int id, int porta ) throws IOException {
        this.porta = porta;
        this.id = id;
        this.grupo = new MulticastSocket( this.porta );
        this.IdCoordenador = -1;
    }
    /**
     * entra no grupo multicast.
     * Inicia a conexão com o grupo.
     * Inicia o thread que trata as respostas do grupo
     * Inicia o thread que verefica se o coordenador existe.
     * @param endereco do grupo.
     */
    public void join( String endereco ) {
        try {
            this.enderecoGrupo = InetAddress.getByName( endereco );
            
            System.out.println("Conectando-se ao grupo");
            this.grupo.joinGroup( this.enderecoGrupo );
            
            System.out.println("Meu ID: " + this.id);
            
            System.out.println("Iniciando leitor");
            this.reader = new Thread( new Reader( this ));
            this.reader.start();
            
            System.out.println("Iniciando verificador de lider !");
            this.verify = new Thread( new Verify(this));
            this.verify.start();
            
            
        } catch (UnknownHostException ex) {
            System.out.println( "NoGrupo: Endereço desconhecido !\n" + ex );
            System.exit(0);
        } catch (IOException ex) {
            System.out.println( "NoGrupo: Erro no canal\n" + ex );
            System.exit(0);
        }
    }
    /**
     * Envia um pacote para o grupo multicast.
     * @param message mensagem no formato: ID:comando:[args]. o ID pode assumir o valor de destino ou -1 quando for para o grupo todo. 
     */
    public void send( String message ) {
        DatagramPacket dt = 
                new DatagramPacket(
                        message.getBytes(), message.length(), 
                        this.enderecoGrupo, this.porta
                );
        try {
            this.grupo.send(dt);
        } catch (IOException ex) {
            System.out.println("NoGrupo: Erro no canal\n" + ex);
            System.exit(0);
        }
    }
    /**
     * Envia um solicitação para o coordenador para entrar no grupo.
     */
    public void joinGrup(){
        this.send("-1:join:" + this.id + ":");
    }
    /**
     * Inicia a eleição enviando o comando para todos os menbros do grupo.
     */
    public void initElection() {
        System.out.println("Iniciando eleção...");
        this.election = true;
        this.won = false;
        this.send("-1:eleiction:" + this.id + ":");  
    }
    /**
     * Classe que verefica se o coordenador está presente, caso contrário inicia a eleição.
     */
    protected class Verify implements Runnable {
        private final NoGrupo noGrupo;

        public Verify(NoGrupo noGrupo) {
            this.noGrupo = noGrupo;
        }
        
        @Override
        public void run() {
            while ( true ) {
                try {
                    
                    this.noGrupo.IdCoordenador = -1;
                    this.noGrupo.joinGrup();
                    
                    int i = 3;
                    while (( i > 0 ) && ( this.noGrupo.IdCoordenador == -1 )) {
                        Thread.sleep(100);
                        i--;
                    }
                    if( (i == 0)  && !this.noGrupo.won && !this.noGrupo.election) {
                        this.noGrupo.initElection();
                        (new Thread( new Wait(this.noGrupo))).start();
                    }
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    System.out.println("NoGrupo: Interrupção inesperada !");
                }
            }
        }
    }
    /**
     * Classe que aguarda a respota de outros menbros no caso de uma eleição, caso os menbros não respondão o mesmo se torna o líder.
     */
    protected  class Wait implements Runnable {
        
        private final NoGrupo noGrupo;
        
        public Wait( NoGrupo noGrupo ) {
            this.noGrupo = noGrupo;
        }
        
        @Override
        public void run() {
            int i = 3;
            while (i > 0) {
                try {
                    Thread.sleep(200);
                    i--;
                } catch (InterruptedException ex) {
                    System.out.println("NoGrupo: Interrupção inesperada !");
                }
            }
            if (!this.noGrupo.won) {
                System.out.println("Se tornando lider !");
                this.noGrupo.coordenador = new Thread( new Coordenador(noGrupo) );
                this.noGrupo.send("-1:coordenador:" + this.noGrupo.id  + ":");
                this.noGrupo.election = false;
                this.noGrupo.iCoordedador = true;
                this.noGrupo.timeCoordenador = new GregorianCalendar();
                this.noGrupo.coordenador.start();
                
            }
                
        }
    }
    /**
     * Classe que trata as respostos do grupo.
     */
    protected class Reader implements Runnable {
        
        private final MulticastSocket grupo;
        private final NoGrupo noGrupo;
        
        public Reader( NoGrupo noGrupo ) {
            this.grupo = noGrupo.grupo;
            this.noGrupo = noGrupo;
        }
        /**
         * Médodo do thread.
         * mensagens direcionais.
         *  caso coordenador: seta o coordenador.
         *  caso message: mensagem de processo externo.
         *  caso won: perdeu a eleição.
         *  caso timestamp: solicita o time do coordenador.
         * mensagens não direcionais.
         *  caso coordenador: um processo ganhou a eleção e se tornou o corrdenador.
         *  caso election: inicia uma eleição caso não tenha iniciado e verefica se ganhou se perdeu nem faz nada só chora.
         *  caso join: se coordenador aceita o menbro no grupo e envia seu ID.
         *  caso getTimestamp: se coordenador retorna o timestamp no momento em que ser tornou coordenador. 
         */
        @Override
        public void run() {
            while ( true ) {
                try {
                    byte[] buffer = new byte[1024];
                    DatagramPacket dp = new DatagramPacket( buffer, buffer.length );
                    this.grupo.receive( dp );
                    
                    String receive = new String( dp.getData() );
                    
                    String[] args = receive.split( ":" );
                    
                    if (args[1] != null ) {
                        if ( Integer.valueOf(args[0]) == this.noGrupo.id ){ 
                            switch ( args[1] ) {
                                case "coordenador" :
                                    this.noGrupo.IdCoordenador = 
                                            Integer.valueOf(new String(args[2]));
                                    break;
                                case "message" :
                                    System.out.println("Cliente: " + args[2]);
                                    break;
                                    
                                case "won" :
                                    if ( this.noGrupo.election && !this.noGrupo.won) {
                                        System.out.println("Perdi a eleição !");
                                        this.noGrupo.won = true;
                                        this.noGrupo.election = false;
                                    }
                                    break;
                                case "timestamp" :
                                    if (this.noGrupo.iCoordedador) {
                                        Calendar c = new GregorianCalendar();
                                        
                                        Long outroTime = new Long(args[2]);

                                        Long meuTime = this.noGrupo.timeCoordenador.getTimeInMillis();
                                        
                                        if (meuTime > outroTime) {
                                            this.noGrupo.iCoordedador = false;
                                            this.noGrupo.coordenador.join();
                                            this.noGrupo.timeCoordenador = null;
                                        }
                                    }
                                    break;
                            }
                        } else if( Integer.valueOf(args[0]) == -1) {
                            switch ( args[1] ) {
                                case "coordenador" : 
                                    this.noGrupo.IdCoordenador = 
                                            Integer.valueOf(args[2]);
                                    this.noGrupo.election = false;
                                    this.noGrupo.won = true;
                                    break;
                                case "eleiction" :
                                    if ( !this.noGrupo.election && !this.noGrupo.won ){
                                        this.noGrupo.initElection();
                                        (new Thread( new Wait(this.noGrupo))).start();
                                    }
                                    int id = Integer.valueOf(args[2]);
                                    if ( id < this.noGrupo.id ) {
                                        this.noGrupo.send(args[2] + ":won:");
                                    }
                                    break;
                                case "join" :
                                    if (this.noGrupo.iCoordedador) {
                                        this.noGrupo.send(args[2] 
                                                + ":coordenador:" 
                                                + this.noGrupo.id + ":"
                                        );
                                    }
                                    break;
                                case "getTimestamp" :  
                                    if (this.noGrupo.iCoordedador) {
                                        this.noGrupo.send(args[2]
                                                + ":timestamp:" 
                                                + this.noGrupo.timeCoordenador.getTimeInMillis()+ ":"
                                        );
                                    }
                                    break;
                            }
                        }
                    }
                    
                } catch (IOException ex) {
                    System.out.println("NoGrupo: Erro no canal\n" + ex);
                    System.exit(1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(NoGrupo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    /**
     * Classe do coordenador que trata as requisições de clientes externos.
     */
    protected class Coordenador implements Runnable{
        
        private final NoGrupo noGrupo;
        private Socket socket;
        private int porta = 8091;
        private String host = "localhost";
        private ServerSocket servidor;
        private List<Thread> clientesThreads;
        
        public Coordenador( NoGrupo noGrupo ) {
            this.noGrupo = noGrupo;
            this.clientesThreads = new ArrayList<>();
            try {
                this.servidor = new ServerSocket(this.porta);
                
                this.socket = new Socket("localhost", 8090);
                PrintStream ps = new PrintStream(this.socket.getOutputStream());
                ps.println("setCoordenador;"
                + this.porta + ";"
                + this.host + ";"
                );
                Thread verf = new Thread(new verifyCoordenador(this));
                verf.start();
            } catch (IOException ex) {
                System.out.println("NoGrupo: Erro no canal TCP: " + ex);
                this.socket = null;
            }
        }
        /**
         * 
         */
        @Override
        public void run() {
            while (this.noGrupo.iCoordedador) {
                try {
                    System.out.println("Aguardando cliente");
                    Socket cliente = this.servidor.accept();
                    System.out.println("Nova conexão com cliente " + 
                    cliente.getInetAddress().getHostAddress()
                    );
                    DealClient deal = new DealClient(cliente, this);
                    Thread t = new Thread(deal);
                    t.start();
                    this.clientesThreads.add(t);
                } catch (IOException ex) {
                    Logger.getLogger(NoGrupo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }  
            try {
                this.servidor.close();
                this.clientesThreads.forEach((thread) -> {
                    try {
                        thread.join();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(NoGrupo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            } catch (IOException ex) {
                Logger.getLogger(NoGrupo.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        /**
         * Classe que trabalho em conjunto com a classe corrdenador para tratar os clientes.
         */
        private class DealClient  implements Runnable{
            private Socket cliente;
            private Coordenador servidor;
        
            public DealClient( Socket cliente, Coordenador servidor ) {
                this.cliente = cliente;
                this.servidor = servidor;
            }
            
            @Override
            public void run(){
                try {
                    Scanner s = new Scanner( this.cliente.getInputStream () );

                    PrintStream saida = new PrintStream(
                            this.cliente.getOutputStream()
                    );

                    while (s.hasNextLine () && this.servidor.noGrupo.iCoordedador) {

                        String receive = s.nextLine();

                        String[] arg = receive.split(";");

                        if (null != arg[0]) switch (arg[0]) {

                            case "mensagem" :
                                this.servidor.noGrupo.send(arg[1]
                                        + ":message:"
                                        + arg[2] + ":"
                                );
                                break;
                            default:
                                saida.println("404");
                        }
                    }
                    
                    s.close();
                    this.cliente.close();
                    
                }   catch (IOException ex) {
                    System.out.println("Erro");
                }
            }
        }
        /**
         * Classe que verefica se existem outros coordenadores na rede.
         */
        private class verifyCoordenador implements Runnable{
            private Coordenador servidor;

            public verifyCoordenador(Coordenador servidor) {
                this.servidor = servidor;
            }
            
            @Override
            public void run() {
                while ( this.servidor.noGrupo.iCoordedador ) {
                    try {
                        Thread.sleep(100);
                        this.servidor.noGrupo.send("-1:getTimestamp:"+
                                this.servidor.noGrupo.id+":");
                    } catch (InterruptedException ex) {
                        System.out.println("NoGrupo: Interrupção inesperada !");
                    }
                }
            }
        }
    }
    /**
     * Se retira do grupo.
     */
    public void leave() {
        try {
            this.grupo.leaveGroup(this.enderecoGrupo);
            this.reader.join();
        } catch (IOException ex) {
            System.out.println("NoGrupo: Erro no canal\n" + ex);
            System.exit(1);
        } catch (InterruptedException ex) {
            System.out.println("Interrupção Inesperada !\n" + ex);
        }
    }
    /**
     * Método principal
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        
        NoGrupo no = new NoGrupo(1, 8090);
        no.join("228.5.6.7");
        
        Scanner s = new Scanner(System.in);
        
        while (s.hasNext() ) {
            no.send(s.nextLine());
        }
        
        s.close();
        no.leave();
    }
}
